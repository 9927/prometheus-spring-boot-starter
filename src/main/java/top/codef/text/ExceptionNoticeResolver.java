package top.codef.text;

import top.codef.exceptions.PrometheusException;
import top.codef.pojos.ExceptionNotice;
import top.codef.pojos.PromethuesNotice;

public interface ExceptionNoticeResolver extends NoticeTextResolver {

	default String resolve(PromethuesNotice notice) {
		if (notice instanceof ExceptionNotice) {
			ExceptionNotice exceptionNotice = (ExceptionNotice) notice;
			return exceptionNoticeResolve(exceptionNotice);
		} else
			throw new PrometheusException("the type of notice is incorrect !");

	}

	String exceptionNoticeResolve(ExceptionNotice exceptionNotice);
}
