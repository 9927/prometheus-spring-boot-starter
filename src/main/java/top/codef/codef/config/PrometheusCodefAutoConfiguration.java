//package top.codef.codef.config;
//
//import java.util.List;
//
//import org.springframework.boot.actuate.health.HealthIndicator;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import top.codef.codef.components.AppFailedListener;
//import top.codef.codef.components.AppReadyListener;
//import top.codef.codef.components.ServicePreRegistListener;
//import top.codef.codef.feign.CodefClient;
//import top.codef.codef.feign.ServiceStateNotice;
//import top.codef.codef.properties.PrometheusCodefCloudProperties;
//
//@Configuration
//@ConditionalOnClass({ HealthIndicator.class })
//@ConditionalOnProperty(name = "prometheus.codef.enabled", havingValue = "true")
//@EnableConfigurationProperties({ PrometheusCodefCloudProperties.class })
//public class PrometheusCodefAutoConfiguration {
//
//	@Bean
//	public ServicePreRegistListener preRegistListener() {
//		return new ServicePreRegistListener();
//	}
//
//	@Bean
//	public AppReadyListener appReadyListener(PrometheusCodefCloudProperties properties,
//			ServicePreRegistListener servicePreRegistListener, List<HealthIndicator> healthIndicators,
//			List<ServiceStateNotice> serviceStateNotices) {
//		return new AppReadyListener(healthIndicators, servicePreRegistListener, properties, serviceStateNotices);
//	}
//
//	@Bean
//	public CodefClient codefClient(ObjectMapper objectMapper,
//			PrometheusCodefCloudProperties prometheusCodefCloudProperties) {
//		return new CodefClient(prometheusCodefCloudProperties, objectMapper);
//	}
//
//	@Bean
//	public AppFailedListener appFailedListener(PrometheusCodefCloudProperties properties,
//			ServicePreRegistListener servicePreRegistListener, List<ServiceStateNotice> serviceStateNotices) {
//		return new AppFailedListener(servicePreRegistListener, properties, serviceStateNotices);
//	}
//
//}
