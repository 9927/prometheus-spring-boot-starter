package top.codef.config.notice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import top.codef.config.conditions.PrometheusEnabledCondition;
import top.codef.httpclient.DingdingHttpClient;
import top.codef.notice.DingDingNoticeSendComponent;
import top.codef.properties.notice.PrometheusNoticeProperties;

@Configuration
@Conditional(PrometheusEnabledCondition.class)
public class DingdingNoticeSendAutoConfig implements NoticeSendComponentCustomer {

	@Autowired
	private PrometheusNoticeProperties prometheusNoticeProperties;

	@Autowired
	private DingdingHttpClient dingdingHttpClient;

	@Override
	public int getOrder() {
		return Ordered.LOWEST_PRECEDENCE - 1;
	}

	@Override
	public void custom(NoticeSendComponentRegister register) {
		prometheusNoticeProperties.getDingding().forEach((x, y) -> {
			DingDingNoticeSendComponent dingDingNoticeSendComponent = new DingDingNoticeSendComponent(
					dingdingHttpClient, y, prometheusNoticeProperties.getDingdingTextType());
			register.add(x, dingDingNoticeSendComponent);
		});
	}

}
