package top.codef.common.abstracts;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.context.ApplicationListener;

import top.codef.exceptionhandle.event.ExceptionNoticeEvent;
import top.codef.exceptionhandle.interfaces.NoticeStatisticsRepository;
import top.codef.notice.INoticeSendComponent;
import top.codef.notice.NoticeComponentFactory;
import top.codef.pojos.NoticeStatistics;
import top.codef.pojos.PromethuesNotice;
import top.codef.properties.frequency.NoticeFrequencyStrategy;
import top.codef.text.NoticeTextResolverProvider;

/**
 * @author 徐晓东
 *
 */
public abstract class AbstractNoticeSendListener implements ApplicationListener<ExceptionNoticeEvent> {

	private final NoticeFrequencyStrategy noticeFrequencyStrategy;

	private final NoticeStatisticsRepository exceptionNoticeStatisticsRepository;

	private final NoticeTextResolverProvider resolverProvider;

	private final NoticeComponentFactory noticeComponentFactory;

	public AbstractNoticeSendListener(NoticeFrequencyStrategy noticeFrequencyStrategy,
			NoticeStatisticsRepository exceptionNoticeStatisticsRepository, NoticeTextResolverProvider resolverProvider,
			NoticeComponentFactory noticeComponentFactory) {
		this.noticeFrequencyStrategy = noticeFrequencyStrategy;
		this.exceptionNoticeStatisticsRepository = exceptionNoticeStatisticsRepository;
		this.resolverProvider = resolverProvider;
		this.noticeComponentFactory = noticeComponentFactory;
	}

	public void send(String who, PromethuesNotice notice) {
		List<INoticeSendComponent> noticeSendComponents = noticeComponentFactory.get(who);
		NoticeStatistics statistics = exceptionNoticeStatisticsRepository.increaseOne(notice);
		if (stratergyCheck(statistics, noticeFrequencyStrategy)) {
			notice.setShowCount(statistics.getShowCount().longValue());
			notice.setCreateTime(LocalDateTime.now());
			noticeSendComponents.forEach(x -> {
				x.send(notice, resolverProvider.get(notice.getClass(), x));
			});
			exceptionNoticeStatisticsRepository.increaseShowOne(statistics);
		}
	}

	/**
	 * @param exceptionStatistics
	 * @param noticeFrequencyStrategy
	 * @return
	 */
	protected boolean stratergyCheck(NoticeStatistics exceptionStatistics,
			NoticeFrequencyStrategy noticeFrequencyStrategy) {
		if (exceptionStatistics.isFirstCreated()) {
			exceptionStatistics.setFirstCreated(false);
			return true;
		}
		boolean flag = false;
		switch (noticeFrequencyStrategy.getFrequencyType()) {
		case TIMEOUT -> {
			Duration dur = Duration.between(exceptionStatistics.getNoticeTime(), LocalDateTime.now());
			flag = noticeFrequencyStrategy.getNoticeTimeInterval().compareTo(dur) < 0;
		}
		case SHOWCOUNT -> {
			flag = exceptionStatistics.getShowCount().longValue() - exceptionStatistics.getLastNoticedCount()
					.longValue() > noticeFrequencyStrategy.getNoticeShowCount().longValue();
		}
		default -> {
		}
		}
		return flag;
	}
}
